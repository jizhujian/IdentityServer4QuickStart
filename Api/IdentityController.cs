﻿using Microsoft.AspNetCore.Mvc;

namespace Api
{
	/// <summary>
	/// 标识
	/// </summary>
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class IdentityController : ControllerBase
	{
		/// <summary>
		/// 用户声明
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public IActionResult GetUserClaims()
		{
			if (User.Identity != null)
				return Ok(User.Claims.Select(it => new { it.Type, it.Value }).ToList());
			return Ok("没有使用用户名密码登录。");
		}
	}
}

