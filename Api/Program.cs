using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Reflection;
using System.Security.Claims;
using System.Text.Json;
using static System.Net.WebRequestMethods;

var builder = WebApplication.CreateBuilder(args);

var authenticationOptions = builder.Configuration.GetSection("Authentication").Get<Api.AuthenticationOptions>()!;
var swaggerUIOptions = builder.Configuration.GetSection("SwaggerUI").Get<Api.SwaggerUIOptions>()!;

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
	.AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
	{
		options.Authority = authenticationOptions.Authority;
		options.RequireHttpsMetadata = authenticationOptions.RequireHttpsMetadata;
		options.Audience = authenticationOptions.Scope;
		// Keycloak client credentials token ��ʾ 401 ����
		options.TokenValidationParameters = new TokenValidationParameters
		{
			ValidateAudience = false,
		};
		// Keycloak ��ɫת��
		options.Events = new JwtBearerEvents()
		{
			OnTokenValidated = context =>
			{
				if (context.Principal?.Identity is ClaimsIdentity identity)
				{
					var realmAccessValue = context.Principal.FindFirst("realm_access")?.Value;
					if (!string.IsNullOrWhiteSpace(realmAccessValue))
					{
						using var realmAccess = JsonDocument.Parse(realmAccessValue);
						if (realmAccess.RootElement.TryGetProperty("roles", out var rolesElement))
						{
							var roles = rolesElement.EnumerateArray();
							foreach (var role in roles)
							{
								var roleValue = role.GetString();
								if (!string.IsNullOrWhiteSpace(roleValue)) identity.AddClaim(new Claim(ClaimTypes.Role, roleValue));
							}
						};
					}
					var resourceAccessValue = context.Principal?.FindFirst("resource_access")?.Value;
					if (!string.IsNullOrWhiteSpace(resourceAccessValue))
					{
						using var resourceAccess = JsonDocument.Parse(resourceAccessValue);
						if (resourceAccess.RootElement.TryGetProperty("account", out var rolesElement))
						{
							var roles = rolesElement.GetProperty("roles").EnumerateArray();
							foreach (var role in roles)
							{
								var roleValue = role.GetString();
								if (!string.IsNullOrWhiteSpace(roleValue)) identity.AddClaim(new Claim(ClaimTypes.Role, roleValue));
							}
						};
					}
				}
				return Task.CompletedTask;
			},
		};
	});

builder.Services.AddCors(o => o.AddPolicy("*", builder => builder.AllowAnyHeader().AllowAnyMethod().SetIsOriginAllowed(_ => true).AllowCredentials()));

builder.Services.AddControllers();
if (swaggerUIOptions.Enabled)
{
	builder.Services.AddEndpointsApiExplorer();
	builder.Services.AddSwaggerGen(options =>
	{
		options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
		{
			Type = SecuritySchemeType.OAuth2,
			Flows = new OpenApiOAuthFlows
			{
				AuthorizationCode = new OpenApiOAuthFlow
				{
					AuthorizationUrl = new Uri($"{authenticationOptions.Authority}{authenticationOptions.AuthorizationUrl}"),
					TokenUrl = new Uri($"{authenticationOptions.Authority}{authenticationOptions.TokenUrl}"),
					Scopes = new Dictionary<string, string> { { authenticationOptions.Scope, swaggerUIOptions.ApiName } }
				}
			}
		});
		options.AddSecurityRequirement(new OpenApiSecurityRequirement {
		{
			new OpenApiSecurityScheme { Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "oauth2" } },
			new[] { authenticationOptions.Scope  }
		}
		});
		options.SwaggerDoc(swaggerUIOptions.ApiVersion, new OpenApiInfo { Title = swaggerUIOptions.ApiName });
		var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
		var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
		options.IncludeXmlComments(xmlPath, true);
	});
};

var app = builder.Build();

var forwardingOptions = new ForwardedHeadersOptions()
{
	ForwardedHeaders = ForwardedHeaders.All
};
forwardingOptions.KnownNetworks.Clear();
forwardingOptions.KnownProxies.Clear();
app.UseForwardedHeaders(forwardingOptions);

app.UseCors("*");

if (swaggerUIOptions.Enabled)
{
	app.UseSwagger();
	app.UseSwaggerUI(options =>
	{
		options.OAuthClientId(swaggerUIOptions.ClientId);
		options.OAuthAppName(swaggerUIOptions.ApiName);
		options.OAuthClientSecret(swaggerUIOptions.ClientSecret);
	});
};

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers()
	.RequireAuthorization();

app.Run();
