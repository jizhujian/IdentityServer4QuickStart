﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WorkerService;

var configuration = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json")
    .Build();
var authenticationOptions = configuration.GetSection("Authentication").Get<AuthenticationOptions>();

var builder = Host.CreateDefaultBuilder(args);

builder.ConfigureServices((hostContext, services) =>
{
	services.AddDistributedMemoryCache();
	services.AddClientCredentialsTokenManagement()
        .AddClient("identityserver", client =>
        {
            client.TokenEndpoint = $"{authenticationOptions.Authority}{authenticationOptions.TokenUrl}";
            client.ClientId = authenticationOptions.ClientId;
            client.ClientSecret = authenticationOptions.ClientSecret;
        });

    services.AddClientCredentialsHttpClient("client", "identityserver", client =>
        {
            client.BaseAddress = new Uri(configuration.GetValue<string>("ApiBaseUrl"));
        });

    services.AddHostedService<Worker>();
});

var app = builder.Build();

app.Run();
