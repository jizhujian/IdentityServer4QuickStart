﻿namespace RazorClient
{
    public class ClaimItem
    {
        public string? Type { get; set; }
        public string? Value { get; set; }
    }
}
