using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using System.Security.Cryptography;
using System.Text.Json;

var builder = WebApplication.CreateBuilder(args);

var authenticationOptions = builder.Configuration.GetSection("Authentication").Get<RazorClient.AuthenticationOptions>();
builder.Services
    .AddAuthentication(options =>
    {
        options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
    })
    .AddCookie(options =>
    {
		options.Cookie.Name = "identityserver4.quickstart.razorclient";
		//net9.0���������޸�
		//options.Events.OnSigningOut = async e =>
  //      {
  //          await e.HttpContext.RevokeRefreshTokenAsync();
  //      };
    })
    .AddOpenIdConnect(options =>
    {
        options.Authority = authenticationOptions.Authority;
        options.RequireHttpsMetadata = authenticationOptions.RequireHttpsMetadata;
        options.Scope.Clear();
        foreach (var scope in authenticationOptions.Scopes.Split(" "))
        {
            options.Scope.Add(scope);
        }
        options.ClientId = authenticationOptions.ClientId;
        options.ClientSecret = authenticationOptions.ClientSecret;
        options.ResponseType = "code";
        options.GetClaimsFromUserInfoEndpoint = true;
        options.SaveTokens = true;
    });

var rsaKey = new RsaSecurityKey(RSA.Create(2048));
var jsonWebKey = JsonWebKeyConverter.ConvertFromRSASecurityKey(rsaKey);
jsonWebKey.Alg = "PS256";
var jwk = JsonSerializer.Serialize(jsonWebKey);

builder.Services.AddOpenIdConnectAccessTokenManagement(options =>
{
	options.DPoPJsonWebKey = jwk;
});

builder.Services.AddUserAccessTokenHttpClient("client", configureClient: client =>
{
    client.BaseAddress = new Uri(builder.Configuration.GetValue<string>("ApiBaseUrl"));
});

// Add services to the container.
builder.Services.AddRazorPages();

var app = builder.Build();

app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapRazorPages()
    .RequireAuthorization();

app.Run();
